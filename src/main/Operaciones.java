package main;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Operaciones extends Remote {
    public int suma(int n1, int n2) throws RemoteException, RemoteException;
}
