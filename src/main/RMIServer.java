package main;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dantl
 */
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;


public class RMIServer extends UnicastRemoteObject implements Operaciones{

    public RMIServer() throws RemoteException {
        super();
    }

    @Override
    public int suma(int n1, int n2) throws RemoteException {
        return n1 + n2;
    }

    public static void main(String[] args){
        try{
            Registry registro = LocateRegistry.createRegistry(7777);
            registro.rebind("RemotoRMI", new RMIServer());
            System.out.println("Servidor Activo");
        }catch (RemoteException e){
            System.out.println(e.getMessage());
        }
    }
}